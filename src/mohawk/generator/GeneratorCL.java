package mohawk.generator;

import java.util.logging.Logger;

public class GeneratorCL {
    public static final Logger logger = Logger.getLogger("mohawk");

    public static void main(String[] args) {
        GeneratorInstance inst = new GeneratorInstance();

        inst.run(args);
    }
}
