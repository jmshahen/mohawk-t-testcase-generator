package mohawk.generator;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

public class GeneratorCUI {
    public static final Logger logger = Logger.getLogger("mohawk");

    public static void main(String[] args) {
        GeneratorInstance inst = new GeneratorInstance();
        ArrayList<String> argv = new ArrayList<String>();
        String cmd = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.print("Enter Commandline Argument ('!exit' to run): ");
        String quotedStr = "";
        while (true) {
            cmd = user_input.next();

            if (quotedStr.isEmpty() && cmd.startsWith("\"")) {
                System.out.println("Starting: " + cmd);
                quotedStr = cmd.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && cmd.endsWith("\"")) {
                System.out.println("Ending: " + cmd);
                argv.add(quotedStr + " " + cmd.substring(0, cmd.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + cmd;
                continue;
            }

            if (cmd.equals("!exit")) {
                break;
            }
            argv.add(cmd);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        inst.run(argv.toArray(new String[1]));
    }

    public static void printCommonCommands() {
        System.out.println("\n\n--- Common Commands ---");
        // {equallyRandom, 1/2Empty, 3/5Empty}
        System.out.println("-random -roles 20 -rules 100 -timeslots 5 -randomPre 1/2Empty "
                + "-comment \"A simple test from the CUI - 1/2 Empty\" !exit");
        System.out.println("-random -roles 20 -rules 100 -timeslots 5 -randomPre 3/5Empty "
                + "-comment \"A simple test from the CUI - 3/5 Empty\" !exit");
        System.out.println("");
        System.out.println(
                "-random -randomSA -roles 100 -rules 200 -timeslots 200 -comment \"Variable 100 roles\" !exit");
        System.out.println(
                "-random -randomSA -roles 300 -rules 200 -timeslots 200 -comment \"Variable 300 roles\" !exit");
        System.out.println(
                "-random -randomSA -roles 500 -rules 200 -timeslots 200 -comment \"Variable 500 roles\" !exit");
        System.out.println(
                "-random -randomSA -roles 700 -rules 200 -timeslots 200 -comment \"Variable 700 roles\" !exit");
        System.out.println(
                "-random -randomSA -roles 1000 -rules 200 -timeslots 200 -comment \"Variable 1000 roles\" !exit");

        System.out.println("");
        System.out.println(
                "-random -randomSA -rules 100 -roles 200 -timeslots 200 -comment \"Variable 100 rules\" !exit");
        System.out.println(
                "-random -randomSA -rules 300 -roles 200 -timeslots 200 -comment \"Variable 300 rules\" !exit");
        System.out.println(
                "-random -randomSA -rules 500 -roles 200 -timeslots 200 -comment \"Variable 500 rules\" !exit");
        System.out.println(
                "-random -randomSA -rules 700 -roles 200 -timeslots 200 -comment \"Variable 700 rules\" !exit");
        System.out.println(
                "-random -randomSA -rules 1000 -roles 200 -timeslots 200 -comment \"Variable 1000 rules\" !exit");

        System.out.println("");
        System.out.println(
                "-random -randomSA -timeslots 100 -roles 200 -rules 200 -comment \"Variable 100 timeslots\" !exit");
        System.out.println(
                "-random -randomSA -timeslots 300 -roles 200 -rules 200 -comment \"Variable 300 timeslots\" !exit");
        System.out.println(
                "-random -randomSA -timeslots 500 -roles 200 -rules 200 -comment \"Variable 500 timeslots\" !exit");
        System.out.println(
                "-random -randomSA -timeslots 700 -roles 200 -rules 200 -comment \"Variable 700 timeslots\" !exit");
        System.out.println(
                "-random -randomSA -timeslots 1000 -roles 200 -rules 200 -comment \"Variable 1000 timeslots\" !exit");
    }
}
